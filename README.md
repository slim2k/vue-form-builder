# vue-form-builder
Primary upstream repository for issues and updates: https://github.com/sethsandaru/vue-form-builder

A simple builder to help you generate a super form for your features/modules/sites,... Easy to use, create, upgrade, maintain,...
Why need to code the form when you can use `Vue-Form-Builder` and render it with some extra steps :D

Advantages:
- Less code in development (No need to do `<form> <div> <input> .... </div> </form>` this by your own)
- Easy to maintain, update your Form in the future.
- Setup your Form with a super friendly UI/UX.
- Extensibility (Your custom control, styling,...)
- Form Validation? I got you fam.
- Included HTML5 structure, no tricks or cheats.

[Vue Form Builder Documentation](https://phattranminh96.gitbook.io/vue-form-builder/)

## Demo
- Demo Online: [Vue Form Builder ONLINE](https://vue-form-builder.herokuapp.com/)
- Demo Form (Real Life Example):
    - [Vue Form Builder Real Life Example](http://vue-form-builder.sethphat.com/)
    - Repo: https://github.com/sethsandaru/demo-vue-form-builder

## Supported Browsers

![Chrome](https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png) | ![Firefox](https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png) | ![IE](https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png) | ![Opera](https://raw.githubusercontent.com/alrra/browser-logos/master/src/opera/opera_48x48.png) | ![Safari](https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png)
--- | --- | --- | --- | --- |
Latest ✔ | Latest ✔ | 9+ ✔ | Latest ✔ | 6.1+ ✔ |

## Documentation

[Main Documentation of Vue-Form-Builder](https://phattranminh96.gitbook.io/vue-form-builder/)

### APIs / Extending / Hard-core stuff

[Visit Documentation](https://phattranminh96.gitbook.io/vue-form-builder/extending/plugin-options)

## License
MIT License
